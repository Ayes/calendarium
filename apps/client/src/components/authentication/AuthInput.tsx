import { forwardRef, memo } from 'react';
import styled from '@emotion/styled';
import { motion } from 'framer-motion';
import { CalendariumTheme } from '../../types/CalendariumTheme.ts';

type AuthInputProps = {
	placeholder: string;
	index?: number;
	[key: string]: unknown;
};

const StyledAuthInput = styled(motion.input)`
	flex: 1;
	padding: 16px;
	color: ${(props) => (props.theme as CalendariumTheme).colors?.text?.paragraphColor};
	font-weight: ${(props) => (props.theme as CalendariumTheme).typography?.h2?.fontWeight};
	position: relative;

	&[type='text'],
	&[type='password'],
	&[type='email'] {
		border: 2px solid transparent;
		border-bottom: 2px solid
			${(props) => (props.theme as CalendariumTheme).colors?.formElements?.inputField?.default.borderColor};

		&:hover {
			border-bottom: 2px solid
				${(props) => (props.theme as CalendariumTheme).colors?.formElements?.inputField?.hovered?.borderColor};
		}

		&:focus {
			border-bottom: 2px solid
				${(props) => (props.theme as CalendariumTheme).colors?.formElements?.inputField?.focused?.borderColor};
		}
	}

	&[type='submit'] {
		border: 2px solid
			${(props) => (props.theme as CalendariumTheme).colors?.formElements?.inputField?.default.borderColor};

		&:hover {
			border: 2px solid
				${(props) => (props.theme as CalendariumTheme).colors?.formElements?.inputField?.hovered?.borderColor};
		}

		&:focus {
			border: 2px solid
				${(props) => (props.theme as CalendariumTheme).colors?.formElements?.inputField?.focused?.borderColor};
		}
	}

	&::placeholder {
		color: rgb(120, 120, 180);
	}

	&.invalid {
		border: 2px solid red;
		animation: shake 0.5s cubic-bezier(0.36, 0.07, 0.19, 0.97) both;
	}

	@keyframes shake {
		10%,
		90% {
			translate: -2px;
		}

		20%,
		80% {
			translate: 2px;
		}

		30%,
		50%,
		70% {
			translate: -4px;
		}

		40%,
		60% {
			translate: 4px;
		}
	}
`;

export const AuthInput = memo(
	forwardRef<HTMLInputElement, AuthInputProps>(({ placeholder, index, ...props }, ref) => {
		return (
			<StyledAuthInput
				initial={{
					opacity: 0,
					x: 100,
				}}
				animate={{
					opacity: 1,
					x: 0,
				}}
				exit={{
					opacity: 0,
					x: -100,
				}}
				transition={{
					delay: (index || 0) * 0.05,
					type: 'spring',
					duration: 1,
					stiffness: 120,
					damping: 14,
				}}
				placeholder={placeholder}
				ref={ref}
				{...props}
			/>
		);
	})
);
