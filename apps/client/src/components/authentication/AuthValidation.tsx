import React from 'react';
import styled from '@emotion/styled';
import { AnimatePresence, motion } from 'framer-motion';
import { AuthRuleList } from './AuthRuleList.tsx';
import { Tab, TabContent } from '../../types/AuthenticationTypes.ts';

const StyledAuthValidation = styled('aside')`
	padding: 5em 1em;
	width: 340px;
`;

type AuthValidationProps = {
	currentFocusedInputField?: TabContent;
	tabs: Tab[];
	activeTab: number;
	[key: string]: unknown;
};

export const AuthValidation: React.FC<AuthValidationProps> = ({
	currentFocusedInputField,
	tabs,
	activeTab,
	...props
}) => {
	return (
		<StyledAuthValidation {...props}>
			<AnimatePresence mode="popLayout">
				{currentFocusedInputField?.isFocused && (
					<motion.p
						initial={{
							x: 50,
							opacity: 0,
						}}
						animate={{
							x: 0,
							opacity: 1,
						}}
						exit={{
							x: 50,
							opacity: 0,
						}}
						transition={{
							type: 'spring',
							duration: 0.5,
							stiffness: 120,
							damping: 14,
						}}
						key={currentFocusedInputField?.key + 'label'}
					>
						{currentFocusedInputField?.description}
					</motion.p>
				)}
			</AnimatePresence>
			<ul>
				<AuthRuleList currentFocusedInputField={currentFocusedInputField} tabs={tabs} activeTab={activeTab} />
			</ul>
		</StyledAuthValidation>
	);
};
