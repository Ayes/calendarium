import { FC, useContext } from 'react';
import styled from '@emotion/styled';
import { CalendariumTheme } from '../types/CalendariumTheme.ts';
import { Link } from 'react-router-dom';
import { UserContext } from '../components/contextProviders/AuthenticationProvider.tsx';

type DashboardPageProps = {
	[key: string]: unknown;
};

const StyledDashboardPage = styled('div')`
	& > header {
		display: flex;

		padding: 0.5em;

		border-bottom: 2px solid
			${(props) => (props.theme as CalendariumTheme).colors?.formElements?.inputField?.default.borderColor};

		& > a {
			display: flex;
			align-items: center;

			padding: 0.5em 0.75em;

			gap: 1em;

			text-decoration: none;

			& > img {
				border-radius: 999vw;
			}

			& > hgroup {
				display: flex;
				flex-direction: column;
				gap: 0.5em;

				& > p {
					font-weight: 700;
					font-size: 0.7rem;
					letter-spacing: 0.1em;
				}

				& > h4 {
					color: ${(props) => (props.theme as CalendariumTheme).colors?.text?.paragraphColor};
				}
			}
		}
	}
`;

export const DashboardPage: FC<DashboardPageProps> = ({ ...props }) => {
	const userContext = useContext(UserContext);
	return (
		<StyledDashboardPage {...props}>
			<header>
				<Link to="#">
					<img src="https://unsplash.it/50/50" alt="profile" />
					<hgroup>
						<p>Logged in as</p>
						<h4>{userContext.userState.displayName}</h4>
					</hgroup>
				</Link>
			</header>
		</StyledDashboardPage>
	);
};
