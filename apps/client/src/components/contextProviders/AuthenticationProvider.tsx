import { createContext, Dispatch, FC, ReactNode, SetStateAction, useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { useLocation, useNavigate } from 'react-router-dom';
import { UserStateType } from '../../types/UserStateType.ts';

export const UserContext = createContext(
	{} as {
		userState: UserStateType;
		setUserState: Dispatch<SetStateAction<UserStateType>>;
	}
);

type AuthContextType = {
	authenticateUser: () => void | Promise<boolean>;
	loginUser: (userId: string, password: string) => boolean | Promise<boolean>;
	registerUser: (displayName: string, userId: string, email: string, password: string) => boolean | Promise<boolean>;
	logoutUser: () => Promise<boolean>;
};

export const AuthContext = createContext({} as AuthContextType);

type AuthenticationProviderProps = {
	children?: ReactNode;
	[key: string]: unknown;
};

export const AuthenticationProvider: FC<AuthenticationProviderProps> = ({ children }) => {
	const [userState, setUserState] = useState({} as UserStateType);
	const navigate = useNavigate();
	const location = useLocation();

	const setTokenCookies = (response: AxiosResponse) => {
		const accessTokenExpiryDate = new Date();
		accessTokenExpiryDate.setDate(accessTokenExpiryDate.getMinutes() + 10);
		Cookies.set('accessToken', response.data.accessToken, { path: '/', expires: accessTokenExpiryDate });

		const refreshTokenExpiryDate = new Date();
		refreshTokenExpiryDate.setDate(refreshTokenExpiryDate.getDate() + 7);
		Cookies.set('refreshToken', response.data.refreshToken, { path: '/', expires: refreshTokenExpiryDate });
	};

	let authenticationRetries = 0;
	const authenticateUser = async (): Promise<boolean> => {
		if (authenticationRetries > 3) {
			authenticationRetries = 0;
			throw Error('Authentication failed with too many retries.');
		}
		try {
			const response = await axios.get('/api/user');
			setUserState((prevState) => ({
				...prevState,
				displayName: response.data.displayName,
				email: response.data.email,
			}));
			return true;
		} catch (authenticationError: unknown) {
			try {
				const response = await axios.post('/api/refresh');

				setTokenCookies(response);

				authenticationRetries += 1;
				await authenticateUser();
				return true;
			} catch (refreshError) {
				if (refreshError instanceof AxiosError) {
					if (refreshError.status !== 200) {
						navigate('/authenticate');
						return false;
					}
					console.error(refreshError);
					return false;
				} else {
					console.error(refreshError);
					return false;
				}
			}
		}
	};

	const loginUser = async (userId: string, password: string): Promise<boolean> => {
		try {
			const response = await axios.post('/api/login', {
				userId: userId,
				password: password,
			});

			setTokenCookies(response);
			return true;
		} catch (error: any) {
			console.error(error);
			return false;
		}
	};
	const registerUser = async (
		displayName: string,
		userId: string,
		email: string,
		password: string
	): Promise<boolean> => {
		try {
			const response = await axios.post('/api/login', {
				displayName: displayName,
				email: email,
				userId: userId,
				password: password,
			});
			setTokenCookies(response);
			return true;
		} catch (error: any) {
			console.error(error);
			return false;
		}
	};

	const logoutUser = async () => {
		try {
			Cookies.remove('accessToken');
			Cookies.remove('refreshToken');
			navigate('/authenticate');
			return true;
		} catch (error) {
			console.error(error);
			return false;
		}
	};

	const [authState] = useState({
		authenticateUser: authenticateUser,
		loginUser: loginUser,
		registerUser: registerUser,
		logoutUser: logoutUser,
	} as AuthContextType);

	useEffect(() => {
		(async () => {
			try {
				await authenticateUser();
			} catch (error) {
				console.error(error);
			}
		})();
	}, [location.pathname]);
	return (
		<UserContext.Provider value={{ userState: userState, setUserState: setUserState }}>
			<AuthContext.Provider value={authState}>{children}</AuthContext.Provider>
		</UserContext.Provider>
	);
};
