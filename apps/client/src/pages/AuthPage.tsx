import { FC, useMemo, useState } from 'react';
import styled from '@emotion/styled';
import { AnimatePresence, LayoutGroup, motion } from 'framer-motion';
import { CalendariumTheme } from '../types/CalendariumTheme.ts';
import { Tab, TabContent } from '../types/AuthenticationTypes.ts';
import { AuthValidation } from '../components/authentication/AuthValidation.tsx';
import { AuthForm } from '../components/authentication/AuthForm.tsx';
import { RootState } from '../stores/store.ts';
import { useSelector } from 'react-redux';

type AuthPageProps = {
	[key: string]: unknown;
};

const StyledAuthPage = styled('div')`
	display: flex;
	justify-content: center;
	padding-top: 5rem;

	& main {
		display: flex;
		flex-direction: column;
		gap: 8px;
		width: 25rem;

		min-height: 550px;

		& > ul {
			display: flex;
			grid-column: 1/2;

			& > li {
				flex: 1;
				cursor: pointer;
				display: flex;
				flex-direction: column;
				background: ${(props) => (props.theme as CalendariumTheme).colors?.button?.focused?.fillColor};
				transition: background-color 200ms;

				&.active {
					background: ${(props) => (props.theme as CalendariumTheme).colors?.button?.focused?.fillColor};
				}

				&:hover {
					background: ${(props) => (props.theme as CalendariumTheme).colors?.button?.hovered?.fillColor};
				}

				& > P {
					padding: 0.5rem 0;
					text-align: center;
					user-select: none;
					font-weight: ${(props) => (props.theme as CalendariumTheme).typography?.h2?.fontWeight};
					flex: 1;
				}

				& > div {
					height: 4px;
					background: ${(props) => (props.theme as CalendariumTheme).colors?.button?.default.fillColor};
				}
			}
		}
	}
`;

const mustBeginWithLowerCase = (input: string) => /^[a-z].*$/.test(input);
const canContainLettersNumbersUnderscoresHyphens = (input: string) => /^[a-z0-9_-]*$/.test(input);
const isValidLength = (input: string) => input.length >= 3 && input.length <= 30;
const containsLowerCase = (input: string) => /[a-z]/.test(input);
const containsUpperCase = (input: string) => /[A-Z]/.test(input);
const containsDigit = (input: string) => /\d/.test(input);
const containsSpecialCharacter = (input: string) => /[@$!%*?&_+.,-]/.test(input);
const isMinimumLength = (input: string) => input.length >= 8;
const matchWithPreviousPassword = (input: string, formValues: TabContent[]) => {
	const passwordInput = formValues?.find((item) => item.key === 'passwordInput');
	return passwordInput ? passwordInput.value === input : false;
};
const isValidEmail = (input: string) => /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(input);
const specialPasswordCharacters = '[ @, $, !, %, *, ?, &, -, _, +, ., , ]';

export const AuthPage: FC<AuthPageProps> = ({ ...props }) => {
	const [activeTab, setActiveTab] = useState(0);
	const languageData = useSelector((state: RootState) => state.language.strings);

	const [tabs, setTabs] = useState([
		{
			label: languageData.authTab.login,
			content: [
				{
					type: 'text',
					placeholder: languageData.loginForm.userId.placeholder,
					key: 'userIdInput',
					isFocused: false,
					description: languageData.loginForm.userId.description,
					value: '',
					rules: [],
				},
				{
					type: 'password',
					placeholder: languageData.loginForm.password.placeholder,
					key: 'passwordInput',
					isFocused: false,
					description: languageData.loginForm.password.description,
					value: '',
				},
				{
					type: 'submit',
					placeholder: languageData.loginForm.submitButton,
					key: 'submitInput',
					isFocused: false,
					description: '',
					value: '',
				},
			],
		},
		{
			label: languageData.authTab.register,
			content: [
				{
					type: 'text',
					placeholder: languageData.registerForm.displayName.placeholder,
					key: 'displayNameInput',
					isFocused: false,
					description: languageData.registerForm.displayName.description,
					value: '',
					rules: [
						{
							description: languageData.registerForm.displayName.mustStartWithLetter,
							checkFunction: (input) => /^[a-zA-z].*$/.test(input),
						},
					],
				},
				{
					type: 'text',
					placeholder: languageData.registerForm.userId.placeholder,
					key: 'userIdInput',
					isFocused: false,
					description: languageData.registerForm.userId.description,
					value: '',
					rules: [
						{
							description: languageData.registerForm.userId.mustStartWithLetter,
							checkFunction: (input) => mustBeginWithLowerCase(input),
						},
						{
							description: languageData.registerForm.userId.canOnlyContain,
							checkFunction: (input) => canContainLettersNumbersUnderscoresHyphens(input),
						},
						{
							description: languageData.registerForm.userId.mustBe3CharsLong,
							checkFunction: (input) => isValidLength(input),
						},
					],
				},
				{
					type: 'email',
					placeholder: languageData.registerForm.email.placeholder,
					key: 'emailInput',
					isFocused: false,
					description: languageData.registerForm.email.description,
					value: '',
					rules: [
						{
							description: languageData.registerForm.email.mustBeValidEmail, // Fill in from new language structure
							checkFunction: (input) => isValidEmail(input),
						},
					],
				},
				{
					type: 'password',
					placeholder: languageData.registerForm.password.placeholder,
					key: 'passwordInput',
					isFocused: false,
					description: languageData.registerForm.password.description,
					value: '',
					rules: [
						{
							description: languageData.registerForm.password.mustContainLowercase,
							checkFunction: (input) => containsLowerCase(input),
						},
						{
							description: languageData.registerForm.password.mustContainUppercase,
							checkFunction: (input) => containsUpperCase(input),
						},
						{
							description: languageData.registerForm.password.mustContainDigit,
							checkFunction: (input) => containsDigit(input),
						},
						{
							description: languageData.registerForm.password.mustContainSpecialChar,
							checkFunction: (input) => containsSpecialCharacter(input),
							additionalData: specialPasswordCharacters,
						},
						{
							description: languageData.registerForm.password.mustBeMinimumLength,
							checkFunction: (input) => isMinimumLength(input),
						},
					],
				},
				{
					type: 'password',
					placeholder: languageData.registerForm.confirmPassword.placeholder,
					key: 'confirmPasswordInput',
					isFocused: false,
					description: languageData.registerForm.confirmPassword.description,
					value: '',
					rules: [
						{
							description: languageData.registerForm.confirmPassword.mustNotBeEmpty, // Fill in from new language structure
							checkFunction: (input) => !!input,
						},
						{
							description: languageData.registerForm.confirmPassword.mustMatch,
							checkFunction: (input, formValues): boolean =>
								matchWithPreviousPassword(input, formValues!),
						},
					],
				},
				{
					type: 'submit',
					placeholder: languageData.registerForm.submitButton,
					key: 'submitInput',
					isFocused: false,
					description: '',
					value: '',
				},
			],
		},
	] as Tab[]);

	const handleBlurAll = () => {
		const newTabs = tabs.map((tab, index) =>
			index !== activeTab
				? tab
				: {
						...tab,
						content: tab.content.map((item) => ({ ...item, isFocused: false })),
				  }
		);
		setTabs(newTabs);
	};

	const currentFocusedInputField = useMemo(
		() => tabs[activeTab].content.find((currentItem) => currentItem.isFocused),
		[activeTab, tabs]
	);

	return (
		<StyledAuthPage {...props}>
			<LayoutGroup>
				<main>
					<ul>
						<AnimatePresence>
							{tabs.map((item, index) => (
								<motion.li
									className={activeTab === index ? 'active' : ''}
									layoutId={item.label}
									initial={{
										y: -100,
									}}
									animate={{
										y: 0,
									}}
									exit={{
										y: -100,
									}}
									transition={{
										delay: index * 0.1,
										type: 'spring',
										duration: 1,
										stiffness: 120,
										damping: 14,
									}}
									key={item.label}
									onClick={() => {
										handleBlurAll();
										setActiveTab(index);
									}}
								>
									<p>{item.label}</p>

									{activeTab === index && <motion.div layoutId="underline" />}
								</motion.li>
							))}
						</AnimatePresence>
					</ul>
					<AuthForm tabs={tabs} activeTab={activeTab} setTabs={setTabs} />
				</main>

				<AuthValidation tabs={tabs} activeTab={activeTab} currentFocusedInputField={currentFocusedInputField} />
			</LayoutGroup>
		</StyledAuthPage>
	);
};
